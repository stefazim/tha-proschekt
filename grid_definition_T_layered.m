
clear;

d=3e-6;
Nx=210;
Ny=630;
h=d/Ny;
x=zeros(1,Nx*Ny);
y=zeros(1,Nx*Ny);
value_bc=zeros(1,Nx*Ny);
type_bc=zeros(1,Nx*Ny,'int8');
neighbors=zeros(Nx*Ny,4);
Epsilon=ones(1,Nx*Ny);
ind=0;
Ezamp=1.;

for i=1:Nx
    
    for j=1:Ny
        ind=ind+1;
        x(ind)=i*h;
        y(ind)=j*h;
        
        % type of bc
%         type_bc(ind)=0;
%         value_bc(ind)=0;
        if(mod((j-mod(j,6))/6,2)==1 && j<=2*Ny/3)
           Epsilon(ind)=-1; 
        elseif(mod((j-mod(j,6))/6,2)==0 && j<=2*Ny/3)
           Epsilon(ind)=10;
        end
        
%         if(j<2*Ny/3-1)
%             Epsilon(ind)=4;
%         end
        if(j<=2*Ny/3 && mod(j,6)==0)
            type_bc(ind)=1;
        end
        
        if(i==Nx/2 && j==2*Ny/3+1)
            type_bc(ind)=3; % ABC
            value_bc(ind)=Ezamp; % value for ABC
        end
        if(i==1)
            type_bc(ind)=4; % ABC
            value_bc(ind)=0; % value for ABC
        end
        if(i==Nx)
            type_bc(ind)=5; % ABC
            value_bc(ind)=0; % value for ABC
        end
        if(j==1)
            type_bc(ind)=6; % ABC
            value_bc(ind)=0; % value for ABC
        end
        if(j==Ny)
            type_bc(ind)=7; % ABC
            value_bc(ind)=0; % value for ABC
        end
                
    end % end - j

end % end - i

% definition of the cells
for i=1:ind
    for j=1:4
        neighbors(ind,j)=0;
    end
end

for j=1:Ny % horizontal neighbors
    yc=j*h;
    ind_p=0;
    for i=1:ind
        if(y(i)==yc)
            if(ind_p>0)
                neighbors(i,1)=ind_p;
                neighbors(ind_p,2)=i;
            end
            ind_p=i;
        end
    end
end
for j=1:Nx % vertical neighbors
    xc=j*h;
    ind_p=0;
    for i=1:ind
        if(x(i)==xc)
            if(ind_p>0)
                neighbors(i,3)=ind_p;
                neighbors(ind_p,4)=i;
            end
            ind_p=i;
        end
    end
end

Ng=ind;
Niter=5000;
E=zeros(1,Nx*Ny);
Hx=zeros(Nx*Ny,2);
Hy=zeros(Nx*Ny,2);
E_old=zeros(1,Nx*Ny);
Hx_old=zeros(Nx*Ny,2);
Hy_old=zeros(Nx*Ny,2);
E_old_old=zeros(1,Nx*Ny);
% Zero initial solution
for i=1:Ng
    if(type_bc(i)==1)
        E(i)=value_bc(i);
    else
        E(i)=0.;
    end
    for j=1:2
        Hx(i,j)=0.;
        Hy(i,j)=0.;
    end
end
